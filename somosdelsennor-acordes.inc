\context ChordNames
	\chords {
	\set chordChanges = ##t

	% intro
	d2 a2 g1
	d2 a2 g1

	% nadie vive ya para si...
	d2 a2 g1
	d2 a2 g1
	d2 a2 g4.. d16 ~ d2
	e2:m g2
	a1 a1:7

	% en vida o mueerteee...
	d4 a4 g2 g4.. d16 ~ d2
	b2:m g2 d4. a8 ~ a2
	b2:m a2 g4.. d16 ~ d2
	b2:m g2 d1 b2:m a2 a1
	d1
	}
