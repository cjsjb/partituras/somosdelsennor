\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble"
		\key d \major

		R1*4  |
%% 5
		fis' 8 fis' g' fis' e' 4 fis' 8 e'  |
		d' 2. r8 d'  |
		fis' 8 fis' g' fis' e' 4 fis' 8 e'  |
		d' 2. r8 d'  |
		fis' 4 a' a' a' 8 a' 16 b' ~  |
%% 10
		b' 8 b' b' a' 16 a' ~ a' 4 r8 fis'  |
		g' 8 fis' 16 e' ~ e' 8 d' d' d' e' fis' ~  |
		fis' 8 e' 2..  |
		r2 r8 a' b' cis''  |
		d'' 4 ( cis'' ) b' b' 8 a'  |
%% 15
		b' 4. a' 16 a' ~ a' 4 r  |
		b' 8 fis' 16 fis' ~ fis' 4 b' b' 8 b'  |
		a' 8 a' fis' 16 fis' e' 4. r4  |
		d'' 8 d'' d'' d'' 16 cis'' 8. b' 8 a' a'  |
		b' 8 a' a' g' 16 a' ~ a' 8 r a' g'  |
%% 20
		fis' 8 d' d' e' fis' d' d' e'  |
		fis' 4 fis' r8 fis' a' g'  |
		fis' 8 d' d' d' e' 2 ~  |
		e' 4 r8 d' d' 2 ~  |
		d' 2 r  |
%% 25
		\bar "|."
	}

	\new Lyrics \lyricsto "soprano" {
		Na -- die vi -- ve ya pa -- ra sí,
		y na -- die mue -- re ya pa -- ra sí.
		Por -- "que el" vi -- vir es vi -- vir pa -- "ra el" Se -- ñor,
		y el mo -- rir, mo -- rir pa -- "ra el" Se -- ñor.

		En vi -- "da o" muer __ te so -- mos del Se -- ñor,
		por lo cual Cris -- to mu -- "rió y" re -- su -- ci -- tó.
		To -- dos es -- ta -- re -- mos en el jui -- "cio an" -- "te el" Se -- ñor,
		"y an" -- te él to -- da ro -- di -- "lla ha" de do -- blar -- se,
		y to -- da len -- "gua ha" "de a" -- la -- bar __
		a Dios. __
	}
>>
