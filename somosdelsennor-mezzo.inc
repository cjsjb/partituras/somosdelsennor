\context Staff = "mezzo" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "mezzo" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble"
		\key d \major

		R1*4  |
%% 5
		fis' 8 fis' g' fis' e' 4 fis' 8 e'  |
		d' 2. r8 a  |
		d' 8 d' e' d' cis' 4 d' 8 cis'  |
		b 2. r8 d'  |
		fis' 4 fis' e' e' 8 e' 16 g' ~  |
%% 10
		g' 8 g' g' fis' 16 fis' ~ fis' 4 r8 fis'  |
		g' 8 fis' 16 e' ~ e' 8 d' d' d' e' fis' ~  |
		fis' 8 e' 2..  |
		r2 r8 e' fis' g'  |
		a' 4 ~ a' g' g' 8 fis'  |
%% 15
		g' 4. fis' 16 fis' ~ fis' 4 r  |
		fis' 8 d' 16 d' ~ d' 4 g' g' 8 g'  |
		fis' 8 fis' fis' 16 fis' e' 4. r4  |
		b' 8 b' b' b' 16 a' 8. g' 8 fis' e'  |
		g' 8 g' g' fis' 16 fis' ~ fis' 8 r fis' e'  |
%% 20
		d' 8 b b cis' d' b b cis'  |
		d' 4 d' r8 d' fis' e'  |
		d' 8 b b b cis' 2 ~  |
		cis' 4 r8 d' d' 2 ~  |
		d' 2 r  |
		\bar "|."
	}

	\new Lyrics \lyricsto "mezzo" {
		Na -- die vi -- ve ya pa -- ra sí,
		y na -- die mue -- re ya pa -- ra sí.
		Por -- "que el" vi -- vir es vi -- vir pa -- "ra el" Se -- ñor,
		y el mo -- rir, mo -- rir pa -- "ra el" Se -- ñor.

		En vi -- "da o" muer __ te so -- mos del Se -- ñor,
		por lo cual Cris -- to mu -- "rió y" re -- su -- ci -- tó.
		To -- dos es -- ta -- re -- mos en el jui -- "cio an" -- "te el" Se -- ñor,
		"y an" -- te él to -- da ro -- di -- "lla ha" de do -- blar -- se,
		y to -- da len -- "gua ha" "de a" -- la -- bar __
		a Dios. __
	}
>>
